# Argument Annotated Essays Paragraphs

This repo contains the [Argument Annotated Essays corpus](https://www.informatik.tu-darmstadt.de/ukp/research_6/data/argumentation_mining_1/argument_annotated_essays/index.en.jsp) split in paragraphs in order to loosely reproduce the text structure found in the [arg-microtexts corpus](http://angcl.ling.uni-potsdam.de/resources/argmicro.html) [2](#reference).

Each essay in the Argument Annotated Essays corpus has the following structure:

![Essay Structure](persuasive_essays.png)

Image taken from [1](#reference)

The structure suggests that each paragraph could form an independent argumentative "sub-text". Therefore, in order to create "microtexts" from this larger compositions we decided to split them accordingly to the paragraph structure. In order to do so we first removed the MajorClaim(s) since the would be the same for each paragraph and therefore do not provide any additional information. Therefore, the "sub-Claim(s)" are taken as major claim for each paragraph. 

Despite the structure presented in the image, there are some instances in which an ADU (Argumentative Discourse Unit), is shared across paragraphs. In this case we duplicate the ADU for each paragraph in which it appears. This is done to keep as the microtext as rich as possible in terms of ADUs.

The repository contains the script used to generate from the Argument Annotated Essays corpus its paragraphed version in the arg-microtext corpus format. The script can be used as follow:
	
	python3 pers_essays2microtext_format.py --dir ./persuasive-essays/brat-project --out ./arg-persuasive-essays

For quick access both the original corpus `persuasive-essays` and the parsed version `arg-persuasive-essays` are included in the repository.

## Requirements

- python 3x
- nltk==3.4.5
- lxml

## Reference

(1) Stab, Christian, and Iryna Gurevych. "Parsing argumentation structures in persuasive essays." Computational Linguistics 43.3 (2017): 619-659.

(2) Andreas Peldszus and Manfred Stede. An annotated corpus of argumentative microtexts. In D. Mohammed, and M. Lewinski, editors, Argumentation and Reasoned Action - Proc. of the 1st European Conference on Argumentation, Lisbon, 2015. College Publications, London, 2016





