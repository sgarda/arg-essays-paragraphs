# Original persuasive essay corpus

In this folder you can find the original corpus containing the persuasive essays. Otherwise you can download it from [here](https://www.informatik.tu-darmstadt.de/ukp/research_6/data/argumentation_mining_1/argument_annotated_essays/index.en.jsp).
