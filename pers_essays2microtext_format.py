#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 15:40:07 2019

@author: Samuele Garda
"""

import argparse
import glob
from nltk.tokenize import sent_tokenize
import numpy as np
from lxml import etree as ET
from collections import defaultdict,OrderedDict,Counter,namedtuple

FU_TO_INT = {"supports" : 1 , "attacks" : -1}
INT_TO_RO  = {1 : "pro", -1 : "opp"}

def parse_arguments():
  
  parser = argparse.ArgumentParser(description='Argumentation Essys to microtext corpus format')
  parser.add_argument('-d','--dir', required = True, help = 'Directory to original corpus and annotations')
  parser.add_argument('-o','--out', required = True, help = 'Directory to store output corpus')
  
  return parser.parse_args()


####################################################################
## IO UTILS
###################################################################

def get_file_base_name(file_name):
  
  base_name = glob.os.path.splitext(glob.os.path.basename(file_name))[0]
  
  return base_name


def make_dir(path):
  
  if not glob.os.path.exists(path):
  
    glob.os.makedirs(path)



####################################################################
## ESSAY UTILS
###################################################################

class TextAnn(namedtuple("TextAnn","id role offset1 offset2 text" )):
  pass

class StanceAnn(namedtuple("StanceAnn","id voice text_id role")):
  pass

class RelAnn(namedtuple("RelAnn","id function id1 id2")):
  pass

class EssayAnn(object):
  
  def __init__(self,annotation_file):
    self.texts = []
    self.stances = []
    self.relations = []
    self.load_elements(annotation_file)
    
  def load_elements(self,anno_file):
    
    with open(anno_file) as infile:
      for line in infile:
        if line.startswith('T'):
          text_ann = line.split()
          id_text,role,offset1,offset2,text = text_ann[0],text_ann[1],text_ann[2],text_ann[3],' '.join(text_ann[4:])
          text_line = TextAnn(id_text,role,offset1,offset2,text)
          self.texts.append(text_line)
          
        elif line.startswith('A'):
          stance_ann = line.split()
          self.stances.append(stance_ann)
        
        elif line.startswith('R'):
          rel_ann = line.replace('Arg1:','').replace('Arg2:','').split()
          self.relations.append(rel_ann)
          

class EssayFile(object):
  
  def __init__(self,essay_file):
    
    self.load_sentences(essay_file)
  
  def load_sentences(self,essay_file):
    
    raw = open(essay_file).read()
    sentences = sent_tokenize(raw)
    self.topic_trigger = sentences.pop(0)
    self.sentences = sentences
    
    
def load_essay_corpus(path):
  
  essays_text = []
  essays_label = []
  
  for file in sorted(glob.glob(glob.os.path.join(path,'*'))):
    with open(file) as infile:
      file_sent,file_label = [],[]
      for line in infile:
        try:
          sent,label = line.split('\t')
          label = label.strip('\n')
        except ValueError:
          values = line.split('\t')
          label = values[-1].strip('\n')
          values.pop(-1)
          sent = ' '.join(values)
          
          
        file_sent.append(sent)
        file_label.append(label)
      
      essays_text.append(file_sent)
      essays_label.append(file_label)
  
  
  return essays_text,essays_label

#########################################################################


###############################################################
### STATS
###############################################################

def in_multiple_subtree(sid,subtrees):
  
  tot = 0
  
  for s in subtrees:
    if sid in s:
      tot += 1
  
  return tot
      

def count_subtree_overlapping_roles(subtree_with_ids,relations):
  
  tot_overlap,tot_overalp_diff_fu = 0,0
  
  all_ids = set(list([el for sublist in subtree_with_ids.values() for el in sublist]))
  
  ids_overlap = [sid for sid in all_ids if in_multiple_subtree(sid,subtree_with_ids.values()) >= 2]
      
  if ids_overlap:
  
    fus_overlap = defaultdict(list)
    
    for sid in ids_overlap:
      
      roles = [rel[1] for rel in relations if rel[2] == sid]
      
      fus_overlap[sid] = roles
        
    fu_overlap_count = {sid : roles.count(roles[0]) == len(roles) for sid,roles in fus_overlap.items()}
    
            
    tot_overlap = len(fu_overlap_count)
  
    tot_overalp_diff_fu = len([k for k,v in fu_overlap_count.items() if not v])
        
  return tot_overlap,tot_overalp_diff_fu
    
  
        
    
#################################################################################

def write_full_essay_xml(base_name,essay_file,essay_anno,outfile):
  
  document = ET.Element('arggraph')
  document.set('id', base_name)
  document.set('topic_id', essay_file.topic_trigger)
  
  conn = 1
  
  # ADD EDUS
  for idx,text in enumerate(essay_anno.texts,start = 1):
    node = ET.SubElement(document, 'edu', id = "e{}".format(idx))
    node.text = ET.CDATA("{}".format(text.text))
    
    if text.role == 'MajorClaim':
      node = ET.SubElement(document, 'adu', id = "a{}".format(idx), type = 'pro')
      
  
  all_adus_roles = {}
  
  # ADD ARGUMENTATION EDGES    
  for idx,rel in enumerate(essay_anno.relations):
    
    src = "a{}".format(rel[2].replace('T',''))
    trg = "a{}".format(rel[3].replace('T',''))
    adu_rel_type = "sup" if rel[1] == 'supports' else "reb"
    all_adus_roles[src] = "pro" if adu_rel_type == 'sup' else 'opp'
    node = ET.SubElement(document, 'edge', src = src, trg = trg, id = "c{}".format(conn), type = adu_rel_type)
    
    conn += 1
  
  # ADD ADUS:
  for name,role in all_adus_roles.items():
    node = ET.SubElement(document, 'adu', id = name, type = role)
      
  # ADD EDU-ADU EDGES
  for idx,text in enumerate(essay_anno.texts,start = 1):
    src = "e{}".format(idx)
    trg = "a{}".format(idx)
    adu_rel_type = "seg"
    node = ET.SubElement(document, 'edge', src = src, trg = trg, id = "c{}".format(conn), type = adu_rel_type)
    
    conn += 1

  et = ET.ElementTree(document)
  
  
  et.write(outfile,encoding='utf-8', xml_declaration=True)
  


def get_children(root,relations):
  
  children = [rel[2] for rel in relations if rel[-1] == root]
  return children


def recursive_walk(root,relations,children):
  for child in get_children(root,relations):
    children.append(child)
    recursive_walk(child,relations,children)
  return children


def get_edge_from_head(head,relations):
  
  return [rel for rel in relations if rel[2] == head][0]


def get_edges_from_buckets(buckets,relations):
  
  subtrees = []
  
  for key,value in buckets.items():
    subtree = []
    subtree.append(get_edge_from_head(key,relations))
    if value:
      for v in value:
        subtree.append(get_edge_from_head(v,relations))
    subtrees.append(subtree)

  return subtrees


def get_edu_from_head(head,essay_anno):
  
  return essay_anno.texts[int(head.replace('T',''))].text


def get_ids_from_rel(claim,relations):
  
  ids = set()
  
  ids.add(claim)
  
  for rel in relations:
    ids.update(rel[-2:])
  
  return sorted(ids)


def is_upmost_parent(curr_id,major_claim):
  
  res = True if curr_id == major_claim else False

  return res


def get_parent(sent_id,relations):
  
  try:
    parent = [rel[-1] for rel in relations if rel[2] == sent_id][0]
  except IndexError:
    raise ValueError("This sent {}\n Has no parent in relations : {}".format(sent_id,relations))
  
  return parent
  

def get_ro_to_int_from_rel(sent_id,claim,lookup,relations):
  
  if sent_id == claim:
    ro = 1
  else:
    ro = lookup.get([rel[1] for rel in relations if rel[2] == sent_id][0])
  
  return ro
  

def get_role(sent_id,major_claim,relations,fu_to_int,int_to_ro):
  

  stack = []
  
  while sent_id != major_claim:
    parent = get_parent(sent_id,relations)
#    print("The parent of {} is {} : with relations : {}".format(sent_id,parent,relations))
    parent_int = get_ro_to_int_from_rel(parent,major_claim,fu_to_int,relations)
    stack.append(parent_int)
    sent_id = parent
    
    
  ro = int_to_ro.get(np.prod(np.asarray(stack)))
  
  return ro


def get_subtree_relations( subtree_ids,relations):
  
  return [rel for rel in relations if rel[2] in subtree_ids]
  
  
def rm_relations_out_of_scope(claim,subtree,relations):
  
  all_ids = set([claim] + subtree)
  
  to_remove = [rel for rel in relations if not (rel[2] in all_ids and rel[3] in all_ids)]
  
  for rm_t in to_remove:
    relations.remove(rm_t)
  
  return relations


def sort_by_id(ids):
  
  no_t = sorted([int(s.replace('T','')) for s in ids])
  
  return ['T' + str(s) for s in no_t]

def get_sent_ids(claim,subtree):
  
  ids = [claim] + subtree
  
  return sort_by_id(ids)
  

def encode_subtree_xml(claim,subtree,essay_anno,base_name,new_file,fu_to_int,int_to_ro):
  
  sent_ids = get_sent_ids(claim,subtree)
        
  subtree_relations = get_subtree_relations(subtree,essay_anno.relations)
  
  subtree_relations = rm_relations_out_of_scope(claim,subtree,subtree_relations)
          
  sent_by_id = OrderedDict((sent.id,sent.text) for sent in essay_anno.texts if sent.id in sent_ids)
  
  oldids2newids = {}
  
  document = ET.Element('arggraph')
  document.set('id', base_name)
  document.set('topic_id', essay_file.topic_trigger)
  
  # ADD EDUS
  for idx,(sent_id,text) in enumerate(sent_by_id.items(),start = 1):
    oldids2newids[sent_id] = idx
    node = ET.SubElement(document, 'edu', id = "e{}".format(idx))
    node.text = ET.CDATA("{}".format(text))
  
  # ADD ADUS with ROLE
  for idx,(sent_id,text) in enumerate(sent_by_id.items(),start = 1):
    if sent_id == claim:
      node = ET.SubElement(document, 'adu', id = "a{}".format(idx), type = 'pro')
    else:
      role = get_role(sent_id,claim,subtree_relations,fu_to_int,int_to_ro)
      node = ET.SubElement(document, 'adu', id = "a{}".format(idx), type = role)
  
  # ADD EDU-ADU EDGES
  conn = 1
  for idx,(sent_id,text) in enumerate(sent_by_id.items(),start = 1):
    src = "e{}".format(idx)
    trg = "a{}".format(idx)
    node = ET.SubElement(document, 'edge', src = src, trg = trg, id = "c{}".format(conn), type = "seg")
    conn += 1
#    
#    
  # ADD ADUS EDGES
  for rel in subtree_relations:
    
    src = "a{}".format(oldids2newids.get(rel[2]))
    trg = "a{}".format(oldids2newids.get(rel[3]))
    adu_rel_type = "sup" if rel[1] == 'supports' else "reb"
    node = ET.SubElement(document, 'edge', src = src, trg = trg, id = "c{}".format(conn), type = adu_rel_type)
    
    conn += 1

  et = ET.ElementTree(document)
  
  et.write(new_file,encoding='utf-8', xml_declaration=True)  
  

def split_into_subtexts(essay_anno,outfile):
    
  major_claim = [t.id for t in essay_anno.texts if t.role == 'MajorClaim'][0]
  
  relations = essay_anno.relations
     
  second_level = sort_by_id(get_children(major_claim,relations))
    
  subtrees_with_ids = defaultdict(list)
  
  # SET CENTRAL CLAIM AND GET SUBTREES
  for sl in second_level:
    recursive_walk(sl,relations,subtrees_with_ids[sl])
    
  
  subtrees_with_ids = OrderedDict((k,subtrees_with_ids.get(k)) for k in second_level)
    
  # REMOVE CLAIMS WITHOUT SUBTREES (CLAIMS CONNECTED TO MAJOR CLAIM)
  subtrees_with_ids = OrderedDict((claim,subtree) for claim,subtree in subtrees_with_ids.items() if len(subtree))
  
  tot_overlap,tot_overalp_diff_fu  = count_subtree_overlapping_roles(subtrees_with_ids,essay_anno.relations)
    

  for idx,(claim,subtree) in enumerate(subtrees_with_ids.items()):
    base_name = get_file_base_name(outfile) + "_{}".format(idx)
    new_file = outfile + "_{}".format(idx) + ".xml"
    encode_subtree_xml(claim,subtree,essay_anno,base_name,new_file,FU_TO_INT,INT_TO_RO)
  
  return tot_overlap,tot_overalp_diff_fu
    
if __name__ == "__main__":
  
  args = parse_arguments()
  
  dir_path = args.dir
  out_path = args.out
  
  make_dir(out_path)
  
  full_texts = sorted(glob.iglob(glob.os.path.join(dir_path,'*.txt')))
  ann_texts = sorted(glob.iglob(glob.os.path.join(dir_path,'*.ann')))
  
  roles_count = Counter()
  
  tot_overlap,tot_overalp_diff_fu = 0,0
  
  for idx,(f_t,a_t) in enumerate(zip(full_texts,ann_texts)):

    base_name = get_file_base_name(f_t)
    print(base_name)
    file_name = glob.os.path.join(out_path,base_name) 
    essay_file = EssayFile(f_t)
    essay_anno = EssayAnn(a_t)
    essay_roles = [rel[1] for rel in essay_anno.relations]
    for r in essay_roles:
      roles_count[r] += 1
    
    essay_tot_overlap,essay_tot_overalp_diff_fu = split_into_subtexts(essay_anno,file_name)
    
    tot_overlap += essay_tot_overlap
    tot_overalp_diff_fu += essay_tot_overalp_diff_fu
  
  print("Role counts : ", roles_count)
 
    
      
